# KiCad Scripts

Scripts I've written to automate laborius tasks in KiCad.

## add_fields

Add a field to all components in a schematic, `manf#` for example.

## calculate_track_lengths

Calculate the length of tracks.

## fix_duplicate_layout_ref

Layout once, copy then rename the references with this script.
