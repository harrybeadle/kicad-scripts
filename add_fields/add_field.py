# Harry Beadle
# 2019-07019

# harrybeadle@protonmail.com

from termcolor import colored
from sys import argv

print("This script adds a field to all components in a KiCad Schematic.")
print(f"Usage:\tpython {argv[0]} <filename> <field to add>")
print()


filename = argv[1]
newfield = argv[2]

print(f"Adding the field '{newfield}' to all components in {filename}.")
print(colored(f"To be safe we'll save the results in {filename.split('.')[0]}.edited.sch!", 'red'))

outputfile = []

IN = "$Comp"
OUT = "$EndComp"
MARKER = 'F'

inComponent = False
added = False
n = None
foundAnyway = False

nedits = 0
ncomps = 0

with open(filename) as file:
    for line in file:
        if IN in line:
            inComponent = True
            ncomps += 1
        if inComponent:
            if line[0] == 'L' and 'power' in line:
                inComponent = False
            if line[0] == MARKER:
                n = int(line.split(' ')[1])
                if newfield in line: # Lazy - *should check only at corrent pos in array 
                    foundAnyway = True
            elif n is not None and n >= 0 and added == False and foundAnyway == False:
                outputfile.append(f"F {n+1} \"\" H 5100 6615 39  0001 C CNN \"{newfield}\"\n")
                added = True
                nedits += 1
        if OUT in line:
            n = None
            inComponent = False
            added = False
            foundAnyway = False
        outputfile.append(line)
        
with open(f"{filename.split('.')[0]}.edited.sch", 'w') as out:
    out.write("".join(outputfile))

print(colored("Finished.", 'green', attrs=['bold']), f"Added the field to {nedits} out of {ncomps} components.")
print("Exit.")
