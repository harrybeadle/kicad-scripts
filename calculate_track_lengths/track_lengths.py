from math import sqrt

def d(a, b):
    return sqrt(a**2 + b**2);

net_lengths = {}

with (open("test.kicad_pcb", 'r')) as file:
    for line in file:
        if "segment" in line and ".Cu" in line:
            my_line = ""
            for c in line:
                if c != ')' and c != '(':
                    my_line += c
            my_numbers = []
            for word in my_line.split(' '):
                try:
                    my_numbers.append(float(word))
                except:
                    pass
            if my_numbers[5] not in net_lengths: net_lengths[my_numbers[5]] = 0
            n = my_numbers
            net_lengths[my_numbers[5]] += d(n[0] - n[2], n[1] - n[3])

for key in net_lengths:
    print(key, net_lengths[key])
