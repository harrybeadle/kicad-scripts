## Why do I need this script?

You've created a hierarchical schematic and used a sheet more then once, because you need multiples of the same circuit on the same board. You layout the circuit once then duplicate it. Now you need to edit all the references so they refer to another sheet's components - this script does that for you.

## How do I use this script?

1. Make sure to annotate your duplicate schematic sheets with the `First free after sheet number X 100` option selected under `Numbering`.
2. Layout your schematic with only the components from the lowest sheet number and delete the components from the other sheets.
3. Duplicate the layout (`CTRL-D` in OpenGL mode).
4. Save the file and close `pcbnew`.
5. Run the script with `python <scriptname>.py <layoutname>.kicad_pcb`.
6. Open `pcbnew` and check the script has done what you expected it to.

## What if it messed up my layout?

The script makes a backup before editing your file at `<filename>.kicad_pcb.<timecode>.old`. Delete the new file and replace it with this to restore your old layout.
