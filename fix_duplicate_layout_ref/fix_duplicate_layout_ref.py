from sys import argv, stdout
from time import time
from shutil import copyfile
import re

if (len(argv) != 2):
    print(
        """
Fix Duplicate Layout References
Harry Beadle

Usage:
    python %s my-pcb.kicad_pcb

Alows you to make a single layout of a sub-schematic in it's own sheet, dupicate it and automatically fix the module references.
        """ % argv[0]
    )
    quit()

filename = argv[1]
backup_filename = argv[1] + "." + str(int(time())) + ".old"


def isToken(word):
    return re.match("[A-Z]\d\d\d", word) != None

copyfile(filename, backup_filename)


tokens = []
contents = ""
contents_n = ""
changes = True
passes = 0

with open(filename, 'r') as file:
    contents_n = file.read()
    while (changes):
        tokens = []
        changes = False
        contents = contents_n
        contents_n = ""
        print("--- Pass %i" % passes)
        for line in contents.split("\n"):
            for word in line.split(" "):
                if isToken(word):
                    if word in tokens:
                        new_token = word[0]+str(int(word[1])+1)+word[2:]
                        contents_n += new_token
                        tokens.append(new_token)
                        changes = True
                        print("Found %s, replaced with %s in pass %i." % (word, new_token, passes))
                    else:
                        contents_n += word
                        tokens.append(word)
                        print("Found new token %s, added to token list." % word)
                else:
                    contents_n += word
                    #print("Found non-token %s, ignored." % word)
                contents_n += " "
            contents_n += "\n"
        passes += 1

with open(filename, 'w') as file:
    file.write(contents_n)

#print (tokens)
